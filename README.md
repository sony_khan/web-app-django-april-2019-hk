# Qwandryx-Palistan

## Requirements

 1. RabbitMQ (https://www.rabbitmq.com/)  
 2. Python 3.6 or Above  
 3. Mysql server  

> On Python 3.7.3 getting issue ```TypeError: GreenSSLSocket does not have a public constructor. Instances are returned by SSLContext.wrap_socket()```
To resolve this issue you may need to use Python 3.6.
 
## DataBase Setup

```json
Database name : myapp
user : root
password : root
```
## Dependencies

From root directory of project
```bash
pip install -r requirements
```

## Running
To run this app 4 separete terminals are required
### terminal 1 (main Server)
```bash
python manage.py makemigrations
python manage.py migrate
python manage.py runsslserver
```

### terminal 2 (celery broker)
```bash
celery -A facebookPages worker --pool=eventlet -l info
```

### terminal 3 (celery beat)

```bash
celery -A facebookPages beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler --pid=
```

### MySQL Commands (mysql)

Run mysql command in mysql terminal in order to fetch third party APIs data without errors

```mysql
ALTER TABLE myapp.dashboard_projectcomment CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE myapp.dashboard_facebook_comment CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE myapp.dashboard_facebook_post CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE myapp.dashboard_facebook_pagetoken CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```
