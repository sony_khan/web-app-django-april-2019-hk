from django.db import models
from accounts.models import User
# Create your models here

class Project(models.Model):
    # TODO: remove user_id redundancy
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255, db_column='title')
    created_at = models.DateTimeField(auto_now_add=True)



class Preferences(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timezone = models.CharField(max_length=255, default='GMT +05 (ISL)', db_column='timezone')
    theme = models.CharField(max_length=255, default='dark', db_column='theme')
    lang = models.CharField(max_length=255, default='en', db_column='lang')
    limit_range = models.CharField(max_length=255, default='20', db_column='limit_range')

class User_fb_token(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    access_token = models.CharField(max_length=255)
    fb_user_name = models.CharField(max_length=255)
    fb_user_email = models.CharField(max_length=255)
    fb_user_picture = models.CharField(max_length=255)





class UserYoutubeToken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    creds = models.CharField(max_length=5000, blank=True)
    token = models.CharField(max_length=5000,  blank=True)
    name = models.CharField(max_length=255,  blank=True)
    image = models.CharField(max_length=255,  blank=True)

class facebook_Pagetoken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    page_access_token = models.CharField(max_length=255)
    page_name = models.CharField(max_length=255)
    page_id = models.CharField(max_length=255)
    page_category = models.CharField(max_length=255)


class facebook_Post(models.Model):
    page = models.ForeignKey(facebook_Pagetoken, on_delete=models.CASCADE)
    post_user_id = models.CharField(max_length=255)
    message = models.TextField()
    post_id = models.CharField(max_length=255)
    created_at = models.CharField(max_length=255)
    post_user_name = models.CharField(max_length=512)
    media_urls = models.TextField()
    likes = models.CharField(max_length=255)
    shares = models.CharField(max_length=255)
    commentcount = models.CharField(max_length=255)


# Change to utf8_bin
class facebook_Comment(models.Model):
    post = models.ForeignKey(facebook_Post, on_delete=models.CASCADE)
    comment_id = models.CharField(max_length=255)
    created_at = models.CharField(max_length=255)
    message = models.TextField()
    comment_user_name = models.CharField(max_length=255)
    comment_user_image = models.TextField(max_length=255)
    comment_user_id = models.CharField(max_length=255)
    comment_likes = models.CharField(max_length=255)
    language = models.CharField(max_length=255,blank=True)
    crisis = models.CharField(max_length=255,blank=True)
    intent = models.CharField(max_length=255,blank=True)
    toxic = models.CharField(max_length=255,blank=True)
    gender = models.CharField(max_length=255,blank=True)

#to get access token for twitter
class User_twitter_token(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    access_key = models.CharField(max_length=255)
    access_secret = models.CharField(max_length=255)
    twitter_user_id = models.CharField(max_length=255)
    twitter_user_name = models.CharField(max_length=255)
    twitter_user_ScreenName = models.CharField(max_length=255)
    twitter_user_picture = models.CharField(max_length=255)

#twitter tweets model
class twitter_tweet(models.Model):

    twitter_account = models.ForeignKey(User_twitter_token, on_delete=models.CASCADE)
    tweet_id = models.CharField(max_length=255)
    text = models.TextField()
    created_at = models.CharField(max_length=255)
    tweet_user_id = models.CharField(max_length=255)
    tweet_user_name = models.CharField(max_length=512)
    tweet_user_ScreenName = models.CharField(max_length=512)
    tweet_user_picture = models.CharField(max_length=255)
    likes = models.CharField(max_length=255 , blank =True)
    language = models.CharField(max_length=255,blank=True)
    crisis = models.CharField(max_length=255,blank=True)
    intent = models.CharField(max_length=255,blank=True)
    toxic = models.CharField(max_length=255,blank=True)
    gender = models.CharField(max_length=255,blank=True)

# twitter comment model
class twitter_replies(models.Model):
    tweet = models.ForeignKey(twitter_tweet, on_delete=models.CASCADE)
    text = models.TextField()
    reply_id = models.CharField(max_length=255)
    created_at = models.CharField(max_length=255)
    reply_user_name = models.CharField(max_length=255)
    reply_user_ScreenName = models.CharField(max_length=512)
    reply_user_image = models.CharField(max_length=255)
    reply_user_id = models.CharField(max_length=255)
    likes = models.CharField(max_length=255)
    language = models.CharField(max_length=255,blank=True)
    crisis = models.CharField(max_length=255,blank=True)
    intent = models.CharField(max_length=255,blank=True)
    toxic = models.CharField(max_length=255,blank=True)
    gender = models.CharField(max_length=255,blank=True)
    
class ProjectComment(models.Model):
    post_ids = models.CharField(max_length=255)

    # TODO: Handle default
    user = models.ForeignKey(User, on_delete=models.CASCADE, default='')
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    #source = models.CharField(max_length=255)
    #message = models.TextField()
    #gender = models.CharField(max_length=255)
    #language = models.CharField(max_length=255)
    #sentiment = models.CharField(max_length=255)
    #user_name = models.CharField(max_length=255)
    #user_image = models.CharField(max_length=255)
    #user_followers = models.CharField(max_length=255)
    #user_id = models.CharField(max_length=255)
    #is_toxic = models.CharField(max_length=255)
    #is_intent = models.CharField(max_length=255)
    #is_crisis = models.CharField(max_length=255)
    #likes = models.CharField(max_length=255)


class YoutubeChannels(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    youtube_id = models.CharField(max_length=255, blank=True)
    title = models.CharField(max_length=255,  blank=True)
    description = models.CharField(max_length=255,  blank=True)
    imurlage = models.CharField(max_length=255,  blank=True)
    status = models.CharField(max_length=255,  blank=True)
    uploads_id = models.CharField(max_length=255,  blank=True)

class YoutubeVideos(models.Model):
    channel = models.ForeignKey(YoutubeChannels, on_delete=models.CASCADE)
    video_id = models.CharField(max_length=255,  blank=True)
    playlistid = models.CharField(max_length=255,  blank=True)
    title = models.CharField(max_length=255,  blank=True)
    description = models.CharField(max_length=5000,  blank=True)
    publishedAt = models.CharField(max_length=255,  blank=True)
    image = models.CharField(max_length=255,  blank=True)

class YoutubeTopLevelComments(models.Model):
    video = models.ForeignKey(YoutubeVideos, on_delete= models.CASCADE)
    commentid = models.CharField(max_length=255,  blank=True)
    name = models.CharField(max_length=255,  blank=True)
    image = models.CharField(max_length=255,  blank=True)
    message = models.CharField(max_length=5000,  blank=True)
    likes = models.CharField(max_length=255,  blank=True)
    time = models.CharField(max_length=255,  blank=True)
    language = models.CharField(max_length=255,blank=True)
    crisis = models.CharField(max_length=255,blank=True)
    intent = models.CharField(max_length=255,blank=True)
    toxic = models.CharField(max_length=255,blank=True)
    gender = models.CharField(max_length=255,blank=True)

class YoutubeSubComments(models.Model):
    top_comment = models.ForeignKey(YoutubeTopLevelComments, on_delete= models.CASCADE)
    commentid = models.CharField(max_length=255,  blank=True)
    name = models.CharField(max_length=255,  blank=True)
    image = models.CharField(max_length=255,  blank=True)
    message = models.CharField(max_length=5000,  blank=True)
    likes = models.CharField(max_length=255,  blank=True)
    time = models.CharField(max_length=255,  blank=True)
    language = models.CharField(max_length=255,blank=True)
    crisis = models.CharField(max_length=255,blank=True)
    intent = models.CharField(max_length=255,blank=True)
    toxic = models.CharField(max_length=255,blank=True)
    gender = models.CharField(max_length=255,blank=True)

