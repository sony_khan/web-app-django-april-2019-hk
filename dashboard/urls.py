from django.urls import path, re_path
from dashboard import views


urlpatterns = [
    path('', views.HomePageView, name='dashboard'),
    re_path(r'^main/(?P<pid>\d+)/$', views.MainView, name='main'),
    re_path(r'^monitoring/(?P<pid>\d+)/$', views.monitoring_dashboard_view, name='monitoring'),
    path('projects', views.projects_view, name='projects'),
    path('create_project', views.create_project, name='create_project'),
    re_path(r'^fb_comment_summary/(?P<postid>\d+)/$', views.CommentFbSummaryView, name='summaryFBComment'),
    re_path(r'^fbcomments/(?P<pid>\d+)/$', views.fbCommentsView, name='fbcomments'),
    re_path(r'^tweets/(?P<tid>\d+)/$', views.twTweetsView, name='tweets'),
    re_path(r'^youtubeData/(?P<channelid>\d+)/$', views.youtubedataView, name='youtubedata'),
    re_path(r'^youtubeData/(?P<channelid>\d+)/(?P<vid>\d+)/$', views.youtubecommentView, name='youtubecomments'),
    re_path(r'^replies/(?P<cid>\d+)/$', views.youtubesubcommentView, name='youtuberplies'),
    re_path(r'^sentiment/(?P<pid>\d+)/$', views.SentimentView, name='sentiment'),
    re_path(r'^crisis/(?P<pid>\d+)/$', views.CrisisPageView, name='crisis'),
    re_path(r'^intent/(?P<pid>\d+)/$', views.IntentPageView, name='intent'),
    re_path(r'^guard/(?P<pid>\d+)/$', views.GuardPageView, name='guard'),
    path('reputation', views.ReputationView, name='reputation'),
    path('finance', views.FinanceView, name='finance'),
    path('hotel', views.HotelView, name='finance'),
    path('arabizi', views.ArabiziView, name='arabizi'),
    path('entity', views.EntityView, name='entity'),
    path('topic', views.TopicView, name='topic'),
    re_path(r'^delete-project/(?P<pid>\d+)/$', views.DeleteProject, name='delete-project'),
    #path('create-project', views.CreateProject, name='create-project'),
    path('filter/results', views.FilterView, name='filter-view'),
    path('saveproject', views.SaveProject, name='save-project'),
    path('usertoken', views.User_fb_TokenView, name='topic'),
    re_path(r'cronjobFB/', views.fbCronJobStart, name='cronjobFB'),
    path('twitter-auth', views.twitterAuth, name='twitter_auth'),
    path('youtube-auth', views.youtubeAuth, name='youtube_auth'),
    path('youtube-callback', views.youtubeCallback, name='youtube_auth_callback'),
    re_path(r'^twitter-callback/$', views.twitterCallback, name='auth_return'), 		#after Oauth to Twitter it will redirect response to views.py -> callable()
    re_path(r'^facebook_auth_handler/$', views.facebook_auth_handler, name='facebook_auth_handler'),

    # User preferences where user can manage their profile
    path('preferences', views.UserPreferences, name='preferences'),

    # path('page', PageView.as_view(), name='page'),
    # path('comment', PageView.as_view(), name='comment'),
    # path('comment', CommentView.as_view(), name='comment'),
    # re_path('page/(?P<page_id>\d+)/$', PageDetailView.as_view(), name='page-detail'),
    # re_path('comments/(?P<page_id>\d+)/$', CommentDetailView.as_view(), name='comment-detail'),
    # path('folder', FolderView.as_view(), name='folder'),
    # re_path('folder/(?P<folder_id>\d+)/$', FolderDetailView.as_view(), name='folder-detail'),

    # For Starting Queue test
    path('addToQueue', views.StartQueue, name = 'startQueue'),
    path('pdf', views.Pdf.as_view()),
    path('csv', views.write_csv, name='csv'),
]