from dashboard.models import   User_twitter_token,twitter_tweet,twitter_replies
import tweepy
from celery import shared_task
import requests
from dashboard.utils import get_api
import logging
# import  dashboard.utils as api

@shared_task
#function to get twitter data
def twitter_Data(userid):
    # get twitter account access information from User_twitter_token
    tweet_token = User_twitter_token.objects.get(user_id=userid)
    #using access key and access secret, get api to get data 
    api = get_api(tweet_token.access_key, tweet_token.access_secret)  # Oauth user
    #to append tweets in json format
    tweets = []
    #to append tweets reply in json format
    replies = []
    #to get user time line tweets
    tweets = api.user_timeline(count=100)
    #fecth respected user tweets replies
    tweetreplies = tweepy.Cursor(api.search, timeout=99999, q='to:{} filter:replies'.format(tweet_token.twitter_user_ScreenName)).items()
    #validation for replies and to handle exception for maximum replies
    while True:
        try:
            reply = tweetreplies.next()
            if not hasattr(reply, 'in_reply_to_user_id_str'):
                continue
            if str(reply.in_reply_to_user_id_str) == tweet_token.twitter_user_id:
                replies.append(reply._json)
        except tweepy.RateLimitError as e:
            logging.error("Twitter api rate limit reached".format(e))
            time.sleep(60)
            continue

        except tweepy.TweepError as e:
            logging.error("Tweepy error occured:{}".format(e))
            break

        except StopIteration:
            break

        except Exception as e:
            logging.error("Failed while fetching replies {}".format(e))
            break

    #funtion to save tweets replies
    save_tweets(tweets, tweet_token)
    #funtion to save tweets replies
    save_replies(replies , tweet_token)
    return True

#to save tweets in db
def save_tweets(tweets, tweet_token):
    for tweet in tweets:
        # to check if tweet not already exist in db
        if not twitter_tweet.objects.filter(tweet_id=tweet.id, twitter_account_id=tweet_token.id).exists():

            newTweet = twitter_tweet()
            newTweet.twitter_account = tweet_token
            newTweet.tweet_id = tweet.id
            newTweet.text = tweet.text
            newTweet.created_at = tweet.created_at
            newTweet.tweet_user_id = tweet.user.id
            newTweet.tweet_user_name = tweet.user.name
            newTweet.tweet_user_ScreenName = tweet.user.screen_name
            newTweet.tweet_user_picture = tweet.user.profile_image_url_https
            newTweet.likes = tweet.favorite_count
            newTweet.save()
    return "tweets saved"

#save tweets replies in database table twitter_replies
def save_replies(replies, tweet_token):
    for reply in replies:
        #to check tweet is available in db
        if twitter_tweet.objects.filter(tweet_id=reply['in_reply_to_status_id_str'],twitter_account_id=tweet_token.id):
            tweet= twitter_tweet.objects.get(tweet_id=reply['in_reply_to_status_id_str'],twitter_account_id=tweet_token.id )
            #to check if reply not already exist in db
            if not twitter_replies.objects.filter(reply_id=reply['id'], tweet_id=tweet.id).exists():
                new_reply = twitter_replies()
                new_reply.tweet = tweet
                new_reply.text = reply['text']
                new_reply.reply_id = reply['id']
                new_reply.created_at = reply['created_at']
                new_reply.reply_user_name = reply['user']['name']
                new_reply.reply_user_ScreenName = reply['user']['screen_name']
                new_reply.reply_user_id = reply['user']['id']
                new_reply.reply_user_image = reply['user']['profile_image_url_https']
                new_reply.likes = reply['favorite_count'] 
                new_reply.save()
    return 'replies saved'
