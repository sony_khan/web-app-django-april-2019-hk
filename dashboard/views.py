from django.views.generic.edit import FormView
from django.views.generic import TemplateView, View
from django.views.generic.base import RedirectView, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.urls import reverse
from django.core import serializers
from datetime import datetime, timedelta
from dateutil.parser import parse
import json
import requests
import time
import tweepy  # API for twitter
from dashboard.models import *
from .tasks import fetchYouData
from dashboard.facebook_task import facebook_PageData, facebook_Custom_PageData
from dashboard.twitter_task import twitter_Data
from django.utils.encoding import smart_bytes, smart_text, force_text
import sys
import os
from django.conf import settings
from django_celery_beat.models import PeriodicTask, IntervalSchedule
import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery
import traceback
from django.db.models import Count
from django.utils.safestring import SafeString
import collections
from dashboard.utils import get_api, genSentChart, genIntentChart, genToxicChart, genCrisisChart, to_datetime, credentials_to_dict
from accounts.models import User
from dashboard.models import Preferences
from django.contrib.auth.hashers import (
    check_password, is_password_usable, make_password,
)
from django import forms
import ast
import os
import json
import re
from django.http import HttpResponseNotFound
from dashboard.youtube_task import youtube_request
from collections import Counter
from django.template.loader import get_template
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa
from django.views.generic import View
from django.utils import timezone
from .models import *
import csv

class Render:
    """
    Render the pdf
    """

    @staticmethod
    def render(path: str, params: dict):
        """
        Generalizeed pdf render
        :param path: template
        :param params: data which wants to print on page
        :return: pdf object
        """
        template = get_template(path)
        html = template.render(params)
        response = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
        if not pdf.err:
            return HttpResponse(response.getvalue(), content_type='application/pdf')
        else:
            return HttpResponse("Error Rendering PDF", status=400)


class Pdf(View):
    """
    Take the view to generate pdf
    """

    def get(self, request):
        """
        Get the request for pdf
        :param request: req object
        :return: object
        """
        params = {
            'today': '1',
            'pings': '1',
            'request': request
        }
        return Render.render('theme/test.html', params)


def write_csv(request):
    with open('demo.csv', mode='w') as result_csv:
        class_csv = csv.writer(result_csv, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        class_csv.writerow(['Name', 'visits', 'page'])
        class_csv.writerow(['Erica Meyers', 'Nov', 'Analitix'])



def MainView(request, pid):

    # Get system user
    active_user = request.user

    posts = facebook_Post.objects.filter(page_id = pid).values_list("post_user_name","message","created_at", "media_urls","likes", "shares","commentcount" , "id")
    # print(posts)
    return render(request, 'theme/fbAllPosts.html', {'pid': pid , "posts":posts, 'active_user': active_user})


def fbCommentsView(request ,pid):
    active_user = request.user
    comments = facebook_Comment.objects.filter(post_id= pid).values_list("comment_user_name","message","created_at","comment_user_image","comment_likes", "language" , "crisis","gender","intent","toxic")
    print(comments)
    return render(request , "theme/fbAllComments.html",{'pid': pid, "pageid":"Page" , "comments":comments, 'active_user': active_user})


def HomePageView(request):
    # Get system user
    active_user = request.user


    # Get FB user and pages
    fb_data = None
    try:
        fb_user_db = User_fb_token.objects.get(user=request.user)
        if(fb_user_db):
            fb_user = {"email" : fb_user_db.fb_user_email , "name" : fb_user_db.fb_user_name , "image":fb_user_db.fb_user_picture}
            fb_pages  = facebook_Pagetoken.objects.filter(user=request.user).values_list('page_name','page_category', 'page_id' , 'id')
            if len(fb_pages) <=0:
                return render(request, 'theme/index.html', {"fb_data" : None})
            fb_post =[]
            for i in range(len(fb_pages)):
                
                fb_post_count = facebook_Post.objects.filter(page_id = fb_pages[i][3]).count()
                fb_lastRun = None
                
                try:
                    fb_lastRun = PeriodicTask.objects.filter(name='pageid_fb'+str(fb_pages[i][3])).values_list('last_run_at')[0]
                    fb_post.append( fb_pages[i] + (fb_post_count, str(fb_lastRun[0])))
                except:
                    fb_post.append( fb_pages[i] + (fb_post_count, None))
        fb_data = {"user": fb_user , "pages": fb_post}
    except:
        pass

    #==================
    # Get Youtube user and pages data
    youtube_data = None
    try:
        youtube_user = UserYoutubeToken.objects.get(user = request.user)

        youtube_data = {}
        youtube_data["user"] = {"name":youtube_user.name , "image":youtube_user.image}
        # channels
        youtube_data["channels"] = []
        channels = YoutubeChannels.objects.filter(user = request.user)
        for channel in  channels:
            count = YoutubeVideos.objects.filter(channel=channel).count()
            youtube_data["channels"].append((channel.title, channel.description, channel.imurlage,channel.id,count))
    except Exception as e:
        print(e)
    #====================
    # Get Tweeter data
    tw_data = None
    try:
        tw_user = User_twitter_token.objects.get(user= request.user)
        print(tw_user.twitter_user_ScreenName, tw_user.twitter_user_picture)
        tw_data = {}
        tw_data["user"] = {"name" : tw_user.twitter_user_ScreenName , "image" : tw_user.twitter_user_picture}
        tw_stat = twitter_tweet.objects.filter(twitter_account = tw_user ).count()
        # print(tw_stat)
        tw_data["tweets"] = [(str(tw_stat) , "None", str(tw_user.id))]
    except Exception as e:
        print(e) 

    #=====================
    return render(request, 'theme/index.html', {"fb_data" : fb_data,"yt_data":youtube_data, "tw_data" : tw_data ,'active_user': active_user})

def twTweetsView(request, tid):

    comments = twitter_tweet.objects.filter(twitter_account_id = tid).values()
    tweets = []
    for tweet in comments:
        replies = twitter_replies.objects.filter(tweet_id = tweet.get('id')).values()
        if len(replies) != 0:
            tweet["replies"] = replies
        tweets.append(tweet)
    return render(request , "theme/twitter/tweeterdata.html",{'pid': None, "comments":comments, 'active_user': request.user})

def CommentFbSummaryView(request, postid):
    print(postid)

    # Get system user
    active_user = request.user
    return render(request , "theme/fbCommentSummary.html",{'pid': postid, "comments":None, 'active_user': active_user})


def monitoring_dashboard_view(request, pid):
    """
    Monitoring dashboard controller
    :param request: gr
    :return: monitoring.html
    """

    # Get system user
    active_user = request.user

    # init
    projects_list = None
    post_ids = None
    total_comments = 0
    positive = 0
    negative = 0
    neutral = 0
    sentiments = []


    # Get the post_ids
    if ProjectComment.objects.filter(user_id=request.user.id, project_id=pid).count() > 0:
        data = ProjectComment.objects.get(user_id=request.user.id, project_id=pid)
        post_ids = re.findall(r'\d+', data.post_ids)

    # Get count of each project comments
    # Add the each count in total_comments

    total_comments = facebook_Comment.objects.filter(post_id__in=post_ids).count()
    posts = facebook_Post.objects.filter(id__in=post_ids)
    fb_avatar = User_fb_token.objects.filter(user_id=request.user.id)

    # TODO: QUEUE's
    # Sentiments Analysis on comments
    # ?lan=en&comment=
    if facebook_Comment.objects.filter(post_id__in=post_ids).count() > 0:
        fb_c = facebook_Comment.objects.filter(post_id__in=post_ids)
        for c in fb_c:
            payload = {'lan': 'en', 'comment': c.message}
            resp = requests.get("http://95.216.2.224:9000/sentiment", params=payload)
            if resp.status_code == 200:
                result = float(resp.json()['Comment:Class'])
                if result == 0:
                    positive += 1
                elif result < 0:
                    negative += 1
                else:
                    neutral += 1



    # appends results
    sentiments.append(positive)
    sentiments.append(negative)
    sentiments.append(neutral)


    # By gender
    male = facebook_Comment.objects.filter(post_id__in=post_ids, gender='Male').count()
    female = facebook_Comment.objects.filter(post_id__in=post_ids, gender='Female').count()
    total = male + female
    m_p = male/total * 100
    f_p = female/total * 100


    return render(request, 'theme/pages/monitoring.html', { 'active_user': active_user,
                                                            'c_count': total_comments,
                                                            'posts': posts,
                                                            'fb_token_data': fb_avatar[0],
                                                            'senti': sentiments,
                                                            'male': m_p,
                                                            'female': f_p})


def projects_view(request):
    """
    Return the project view
    :param request:
    :return: project.html
    """

    # Get system user
    active_user = request.user

    # Init
    projects = None

    # Filter
    if Project.objects.filter(user_id=request.user.id).count() > 0:
        projects = Project.objects.filter(user_id=request.user.id)


    return render(request, 'theme/pages/projects.html', { 'active_user': active_user, 'projects': projects})


def create_project(request):
    """
    Create a new project using axios
    Desc: fbAllPosts.html
    :param request:
    :return: HTTTP_STATUS_CODE 200, 404
    """

    if request.method == 'POST':

        # Retrieve data
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        title = body['title']
        aIds = body['post_ids']
        user_id = request.user.id


        # Create instance
        pro = Project.objects.create(title=title, user_id=user_id)


        # Generate entries for c_ids
        # parse the JSON stringify
        ProjectComment.objects.create(post_ids=aIds, project_id=pro.id, user_id=user_id)

        return HttpResponse(request.body, status=200)

    # Prevent direct access
    return HttpResponseNotFound()


def pretty_request(request):
    """
    Return the request in the print format
    :param request: gr
    :return: formatted string
    """
    headers = ''
    for header, value in request.META.items():
        if not header.startswith('HTTP'):
            continue
        header = '-'.join([h.capitalize() for h in header[5:].lower().split('_')])
        headers += '{}: {}\n'.format(header, value)

    return (
        '{method} HTTP/1.1\n'
        'Content-Length: {content_length}\n'
        'Content-Type: {content_type}\n'
        '{headers}\n\n'
        '{body}'
    ).format(
        method=request.method,
        content_length=request.META['CONTENT_LENGTH'],
        content_type=request.META['CONTENT_TYPE'],
        headers=headers,
        body=request.body,
    )


def youtubedataView(request, channelid):
    channel = YoutubeChannels.objects.get(id = channelid)
    videos = YoutubeVideos.objects.filter(channel_id = channelid).values_list('image','title','description','publishedAt', 'id')
    print(videos)

    return render(request , "theme/youtube/videos.html",{'title': channel.title, "videos":videos, 'active_user': request.user})

def youtubecommentView(request,channelid , vid):
    video = YoutubeVideos.objects.get(id = vid)
    comments = YoutubeTopLevelComments.objects.filter(video_id = vid).values()
    commNsub =[]
    for comment in comments:
        sub = YoutubeSubComments.objects.filter(top_comment_id= comment['id']).values()
        if(len(sub) > 0):
            comment["replies"] = list(sub)
        commNsub.append(comment)
    return render(request , "theme/youtube/topComments.html",{'title': video.title, "comments":commNsub})
def youtubesubcommentView(request,cid):
    print(cid)
    comments = YoutubeSubComments.objects.filter(top_comment_id= cid).values()
    print(comments)
    if(len(comments)<=0):
        return HttpResponse("No Replies Found")
    return render(request , "theme/youtube/replies.html",{'title': "Title", "comments":comments})
    
def SentimentView(request, pid):
    pro = Project.objects.get(id=pid)
    projects = Project.objects.filter(user=request.user)
    page = facebook_Pagetoken.objects.filter(user=request.user)[0]

    comments = ProjectComment.objects.filter(project=pro)
    postids = comments.values('post_id').annotate(dcount=Count('post_id'))
    posts = list()
    for pst in postids:
        tempst = facebook_Post.objects.get(id=pst['post_id'])
        posts.append(tempst)

    filtercounts = createFilter(comments)
    chartdata = genSentChart(comments)    

    return render(request, 'core/sentiment.html', {'chartdata': chartdata,'posts':posts, 'comments': comments,'page':page, 'project': pro, 'projects': projects, 'filterdata': filtercounts})


def CrisisPageView(request, pid):
    projects = Project.objects.filter(user=request.user)
    page = facebook_Pagetoken.objects.filter(user=request.user)[0]
    pro = Project.objects.get(id=pid)

    comments = ProjectComment.objects.filter(project=pro)
    crisiscomments = comments.filter(is_crisis='Problematic')
    nocrisiscomments = comments.filter(is_crisis='Non Problematic')
    numnocrisiscomments = comments.filter(is_crisis='Non Problematic').count()
    numcrisiscomments = comments.filter(is_crisis='Problematic').count()
    postids = crisiscomments.values('post_id').annotate(dcount=Count('post_id'))
    posts = list()
    for pst in postids:
        tempst = facebook_Post.objects.get(id=pst['post_id'])
        posts.append(tempst)

    filtercounts = createFilter(crisiscomments)
    filtercounts['nocrisis'] = numnocrisiscomments
    filtercounts['crisis'] = numcrisiscomments

    chartdata = genCrisisChart(crisiscomments)

    return render(request, 'core/crisis.html', {'project': pro,'posts':posts, 'projects': projects,'page':page, 'chartdata': chartdata, 'comments': crisiscomments, 'filterdata': filtercounts,'totalcoms':comments.count()})


def IntentPageView(request, pid):
    projects = Project.objects.filter(user=request.user)
    page = facebook_Pagetoken.objects.filter(user=request.user)[0]

    pro = Project.objects.get(id=pid)
    comments = ProjectComment.objects.filter(project=pro)
    intcoms = comments.filter(is_intent='True')
    num_non_int_com = comments.filter(is_intent='False').count()
    num_int_com = comments.filter(is_intent='True').count()
    postids = intcoms.values('post_id').annotate(dcount=Count('post_id'))
    posts = list()
    for pst in postids:
        tempst = facebook_Post.objects.get(id=pst['post_id'])
        posts.append(tempst)

    filtercounts = createFilter(intcoms)
    filtercounts['nointent'] = num_non_int_com
    filtercounts['intent'] = num_int_com

    chartdata = genIntentChart(intcoms)

    return render(request, 'core/intent.html', {'project': pro,'posts':posts, 'projects': projects,'page':page, 'chartdata': chartdata, 'comments': intcoms, 'filterdata': filtercounts,'totalcoms':comments.count()})


def GuardPageView(request, pid):
    projects = Project.objects.filter(user=request.user)
    page = facebook_Pagetoken.objects.filter(user=request.user)[0]
    pro = Project.objects.get(id=pid)
    comments = ProjectComment.objects.filter(project=pro)
    toxcoms = comments.filter(is_toxic='True')
    nontoxcoms = comments.filter(is_toxic='False')
    num_non_tox_com = comments.filter(is_toxic='False').count()
    num_tox_com = comments.filter(is_toxic='True').count()
    postids = toxcoms.values('post_id').annotate(dcount=Count('post_id'))
    posts = list()
    for pst in postids:
        tempst = facebook_Post.objects.get(id=pst['post_id'])
        posts.append(tempst)

    filtercounts = createFilter(toxcoms)
    filtercounts['notoxic'] = num_non_tox_com
    filtercounts['toxic'] = num_tox_com
    chartdata = genToxicChart(toxcoms)

    return render(request, 'theme/commingsoon.html', {'project': pro,'posts':posts, 'projects': projects,'page':page, 'chartdata': chartdata, 'comments': toxcoms, 'filterdata': filtercounts,'totalcoms':comments.count()})


def ReputationView(request):
    return render(request, 'theme/commingsoon.html')


def FinanceView(request):
    return render(request, 'theme/commingsoon.html')


def HotelView(request):
    return render(request, 'theme/commingsoon.html')


def ArabiziView(request):
    return render(request, 'theme/commingsoon.html')


def EntityView(request):
    return render(request, 'theme/commingsoon.html')


def TopicView(request):
    return render(request, 'theme/commingsoon.html')


def DeleteProject(request, pid):
    pro = Project.objects.get(id=pid)
    pro.delete()
    return redirect('/dashboard/projects')


def CreateProject(request):
    if request.method == 'POST':
        pro = Project()
        pro.title = request.POST['project_name']
        pro.notification_duration = 8
        pro.user = request.user
        pro.save()
        mainurl = '/dashboard/main/{}'.format(pro.id)
        comments = facebook_Comment.objects.filter(user=request.user)
        for com in comments:
            newcom = ProjectComment()
            newcom.message = com.message
            newcom.post_id = com.post_id 
            newcom.source = com.source
            newcom.comment_id = com.comment_id
            newcom.language = com.language
            newcom.sentiment = com.sentiment
            newcom.user_name = com.user_name
            newcom.project = pro
            newcom.likes = com.likes 
            newcom.user_image = com.user_image
            newcom.user_followers = com.user_followers
            newcom.user_id = com.com_user_id
            newcom.gender = com.gender
            newcom.is_toxic = com.is_toxic
            newcom.is_intent = com.is_intent
            newcom.is_crisis = com.is_crisis
            newcom.created_at = com.created_at
            newcom.save()
        return redirect(mainurl)
    return redirect('/')

# connecting facebook using fb access token and then getting pages access token and save into pagetoken table
def User_fb_TokenView(request):
    # authentication token coming from jquery function exist in template/core/index.html
    tok = json.loads(request.body.decode('utf-8'))
    token = tok['token']
    # getting access token through this graph api call
    tokenurl = "https://graph.facebook.com/v3.2/oauth/access_token?grant_type=fb_exchange_token&client_id=188312881935144&client_secret=57ee2101ea55a4e93b6ce36a6cdee00b&fb_exchange_token={}".format(
        token)
    r = requests.get(tokenurl).json()
    # here r having data of access token
    newtoken = r['access_token']
    #getting fb user information, email and name
    userInfo=  requests.get('https://graph.facebook.com/me/?fields=email,name,picture&access_token=' +newtoken).json()
    if userInfo.get('error', False):
        raise Exception('facebook user data not accessing')
    
    #  getting pages information that user select when connecting his facebook account
    pages = requests.get(
        'https://graph.facebook.com/me/accounts?access_token='+newtoken).json()
    if pages.get('error', False):
        raise Exception('facebook')
    twits = pages.get('data')
    # If user connects with facebook again, we delete already available pages info of user 
    facebook_Pagetoken.objects.filter(user=request.user).delete()
    # pagetoken is database entity where we save page names,access_token, page_id, page_category, user_id as foriegen key
    for twit in twits:
        newtwit = facebook_Pagetoken()
        newtwit.user = request.user
        newtwit.page_id = twit['id']
        newtwit.page_access_token = twit.get('access_token','')
        newtwit.page_name = twit.get('name','')
        newtwit.page_category = twit.get('category','')
        newtwit.save()
    #  saving user info and user fb access token in User_fb_token table
    # in try block updating user data
    try:
        token = User_fb_token.objects.get(user=request.user.id)
        token.user = request.user
        token.access_token = newtoken
        token.fb_user_name= userInfo.get('name','')
        token.fb_user_email= userInfo.get('email','')
        token.fb_user_picture= userInfo.get('picture',{}).get('data',{}).get('url', '')
        token.save()
    #  in expect block if already do not exit user data, insert and save it
    except User_fb_token.DoesNotExist:
        newtok = User_fb_token()
        newtok.user = request.user
        newtok.access_token = newtoken
        newtok.fb_user_name= userInfo.get('name','')
        newtok.fb_user_email= userInfo.get('email','')
        newtok.fb_user_picture= userInfo.get('picture',{}).get('data',{}).get('url', '')
        newtok.save()
    print("IN FB TOKEN")
    facebook_PageData.delay(request.user.id)

    # test for custom function
    # facebook_Custom_PageData.delay(page_name="Sketch Book", since='2018-01-01')
    return JsonResponse('success', safe=False)

def SaveProject(request):
    resp = json.loads(request.body.decode('utf-8'))
    coms = resp['comments']
    projectid = resp['projectid']
    objects = ProjectComment.objects.filter(project_id=projectid).exclude(comment_id__in=coms).delete()    
    return JsonResponse('success', safe=False)

def FilterView(request):

    # Code Used for Comments
    params = json.loads(request.body.decode('utf-8'))
    com = {}
    project = Project.objects.get(id=params['pro_id'])

    comments = ProjectComment.objects.filter(project_id=params['pro_id'])

    datadict = dict()

    if params['page'] == 'intent':
        comments = comments.filter(is_intent='True')
    if params['page'] == 'crisis':
        comments = comments.filter(is_crisis='Problematic')
    if params['page'] == 'toxic':
        comments = comments.filter(is_toxic='True')

    if len(params['social']) > 0:
        comments = comments.filter(source__in=params['social'])
    if len(params['lng']) > 0:
        comments = comments.filter(language__in=params['lng'])
    if len(params['gen']) > 0:
        comments = comments.filter(gender__in=params['gen'])
    if params['sent'] != '':
        comments = comments.filter(sentiment=params['sent'])
    if params['daterange'] != '':
        strarr = params['daterange'].split('-')
        tempcoms = list()
        for com in comments:
            if(com.source == 'twit'):
                comdate = to_datetime(com.created_at)
            else:
                comdate = datetime.strptime(
                    com.created_at, '%Y-%m-%d %H:%M:%S')
         
            startdate = datetime.strptime(strarr[0].strip(), '%m/%d/%Y')
            enddate = datetime.strptime(strarr[1].strip(), '%m/%d/%Y')
            if comdate > startdate and comdate < enddate:
                tempcoms.append(com.id)

        comments = comments.filter(id__in=tempcoms)

    if len(params['date']) > 0:
        tempcoms = list()
        for com in comments:
            if com.source == 'twit':
                comdate = to_datetime(com.created_at)
            else:
                comdate = datetime.strptime(
                    com.created_at, '%Y-%m-%d %H:%M:%S')
            print(comdate)
            d2 = datetime.now()
            delta = d2 - comdate
            print(params['date'])
            for date in params['date']:
                if delta.days <= int(date):
                    tempcoms.append(com.id)
                    break
        comments = comments.filter(id__in=tempcoms)


    if params['page'] == 'sentiment':
        chartdata = genSentChart(comments)
        data = serializers.serialize("json", comments)
        datadict['chartdata'] = chartdata

    if params['page'] == 'intent':
        chartdata = genIntentChart(comments)
        data = serializers.serialize("json", comments)
        datadict['chartdata'] = chartdata

    if params['page'] == 'crisis':
        chartdata = genCrisisChart(comments)
        data = serializers.serialize("json", comments)
        datadict['chartdata'] = chartdata

    if params['page'] == 'toxic':
        chartdata = genToxicChart(comments)
        data = serializers.serialize("json", comments)
        datadict['chartdata'] = chartdata

    if params['page'] == 'main':
        temp = list()
        for comment in comments:
            temp.append(comment.language)

        langlabel = list()
        langvalue = list()
        for key, lang in collections.Counter(temp).items():
            langlabel.append(key)
            langvalue.append(lang)

        datadict['langlabel'] = langlabel
        datadict['langvalue'] = langvalue
    
    postids = comments.values('post_id').annotate(dcount=Count('post_id'))
    posts = list()
    for pst in postids:
        tempst = facebook_Post.objects.get(id=pst['post_id'])
        posts.append(tempst)

    comms = serializers.serialize("json", comments)
    posts = serializers.serialize("json", posts)
    datadict['comments'] = comms
    datadict['posts'] = posts

    return JsonResponse(datadict, safe=False)


def createFilter(comments):
    filtercounts = dict()
    filtercounts['nomale'] = comments.filter(gender='Male').count()
    filtercounts['nofemale'] = comments.filter(gender='Female').count()

    filtercounts['nofb'] = comments.filter(source='fb').count()
    filtercounts['notwit'] = comments.filter(source='twit').count()
    filtercounts['noyou'] = comments.filter(source='youtube').count()
    filtercounts['noins'] = comments.filter(source='insta').count()

    filtercounts['nopos'] = comments.filter(sentiment='Positive').count()
    filtercounts['noneg'] = comments.filter(sentiment='Negative').count()
    filtercounts['nonue'] = comments.filter(sentiment='Neutral').count()

    filtercounts['encom'] = comments.filter(language='en').count()
    filtercounts['frcom'] = comments.filter(language='fr').count()
    filtercounts['arcom'] = comments.filter(language='ar').count()
    filtercounts['arzcom'] = comments.filter(language='arz').count()

    return filtercounts


def twitterCallback(request):
    verifier = request.GET.get('oauth_verifier')
    oauth = tweepy.OAuthHandler(settings.CONSUMER_KEY, settings.CONSUMER_SECRET)
    oauth.request_token = request.session['request_token']
    request.session.delete('request_token')

    oauth.get_access_token(verifier)
    #using access key and access secret, get api to get data 
    api = get_api(oauth.access_token, oauth.access_token_secret)  # Oauth user
    # getting account user information
    account_user=api.me()
    account_user=account_user._json

    #save account access data in database, or update if already available
    try:
        twit = User_twitter_token.objects.get(user=request.user.id)
        twit.user = request.user
        twit.access_key = oauth.access_token
        twit.access_secret = oauth.access_token_secret
        twit.twitter_user_name = account_user.get('name','')
        twit.twitter_user_ScreenName = account_user.get('screen_name','')
        twit.twitter_user_id = account_user.get('id','')
        twit.twitter_user_picture = account_user.get('profile_image_url_https','')
        twit.save()

    except User_twitter_token.DoesNotExist:
        newtwit = User_twitter_token()
        newtwit.user = request.user
        newtwit.access_key = oauth.access_token
        newtwit.access_secret = oauth.access_token_secret
        newtwit.twitter_user_name = account_user.get('name','')
        newtwit.twitter_user_ScreenName = account_user.get('screen_name','')
        newtwit.twitter_user_id = account_user.get('id','')
        newtwit.twitter_user_picture = account_user.get('profile_image_url_https','')
        newtwit.save()

    twitter_Data.delay(request.user.id)

    return redirect('/dashboard')


def twitterAuth(request):
    request.session.delete('request_token')
    # start the OAuth process, set up a handler with our details
    oauth = tweepy.OAuthHandler(settings.CONSUMER_KEY, settings.CONSUMER_SECRET)
    # direct the user to the authentication url
    # if user is logged-in and authorized then transparently goto the callback URL
    auth_url = oauth.get_authorization_url(True)
    # print(auth_url)
    # store the request token
    request.session['request_token'] = oauth.request_token

    return HttpResponseRedirect(auth_url)

def youtubeAuth(request):
     # Create flow instance to manage the OAuth 2.0 Authorization Grant Flow steps.
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        settings.CLIENT_SECRETS_FILE, scopes=settings.SCOPES)

    # The URI created here must exactly match one of the authorized redirect URIs
    # for the OAuth 2.0 client, which you configured in the API Console. If this
    # value doesn't match an authorized URI, you will get a 'redirect_uri_mismatch'
    # error.
    flow.redirect_uri = request.build_absolute_uri(reverse('youtube_auth_callback'))
    authorization_url, state = flow.authorization_url(
        # Enable offline access so that you can refresh an access token without
        # re-prompting the user for permission. Recommended for web server apps.
        access_type='offline',
        # Enable incremental authorization. Recommended as a best practice.
        include_granted_scopes='true')
    # Store the state so the callback can verify the auth server response.
    request.session['state'] = state

    return redirect(authorization_url)

def youtubeCallback(request):
    # Specify the state when creating the flow in the callback so that it can
    # verified in the authorization server response.
    state = request.session['state']

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(settings.CLIENT_SECRETS_FILE, scopes=settings.SCOPES, state=state)
    flow.redirect_uri = request.build_absolute_uri(reverse('youtube_auth_callback'))
    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = request.build_absolute_uri()

    flow.fetch_token(authorization_response=authorization_response)

    # Store credentials in the session.
    # ACTION ITEM: In a production app, you likely want to save these
    # credentials in a persistent database instead.

    credentials = flow.credentials
    dictcreds = credentials_to_dict(credentials)
    print(dictcreds["token"])
    result = requests.get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token="+dictcreds["token"]).json()

    youtoken , created= UserYoutubeToken.objects.get_or_create(user=request.user)
    print(youtoken)
    # youtoken.user = request.user
    youtoken.creds = str(dictcreds) 
    youtoken.token = dictcreds["token"]
    youtoken.name = result.get("name" , "")
    youtoken.image = result.get("picture", "")
    youtoken.save()

    print("HEre Is it")
    
    youtube_request(request.user.id )
    # fetchYouData.delay(request.user.id)
    return redirect('dashboard')


def facebook_auth_handler(request):
    # code = request.GET.get("code")
    # if not code:
    #     return redirect('/')
    # try:
    #     access_token = graph.get_access_token_from_code(code, settings.FACEBOOK_CONFIG['redirect_uri'],
    #                                                     settings.FACEBOOK_CONFIG['app_id'],
    #                                                     settings.FACEBOOK_CONFIG['app_secret'])
    #     if not access_token:
    #         return redirect('/')
    #     graph.access_token = access_token['access_token']
    #     request.session['fb_access_token'] = access_token['access_token']
    # except Exception as e:
    #     print(e)
    #     traceback.print_exc()
    return redirect('/')


def UserPreferences(request):

    # Fetch the active user
    active_user = User.objects.get(email=request.user)
    pref = ''

    # On get fetch all require data
    if request.method == 'GET':
        active_user = User.objects.get(email=request.user)
        # Check if your don't have preference once signup
        # If not create new default data for preferences
        pref = Preferences.objects.filter(user_id=active_user.id)
        if not pref:
            Preferences.objects.create(user_id=active_user.id)
            redirect('dashboard/preferences')
        else:
            pref = Preferences.objects.get(user_id=active_user.id)
            return render(request, 'core/preferences.html', {'user': active_user, 'pref': pref})

    # Handle POST request
    if request.method == 'POST':

        # Fetch preferences
        pref = Preferences.objects.get(user_id=active_user.id)

        # Handle password request
        #matchPassword = check_password(request.POST.get('password'), active_user.password)
        if request.POST.get('password'):
            #if not matchPassword:
            User.objects.filter(email=active_user.email).update(
                first_name=request.POST.get('first_name'),
                last_name=request.POST.get('last_name'),
                password=make_password(request.POST.get('password'))
            )
        else:
            User.objects.filter(email=active_user.email).update(
                first_name=request.POST.get('first_name'),
                last_name=request.POST.get('last_name')
            )

        # Update preferences
        Preferences.objects.filter(user_id=active_user.id).update(
            lang=request.POST.get('lang'),
            theme=request.POST.get('theme'),
            timezone=request.POST.get('timezone'),
            limit_range=request.POST.get('limit_range')
        )

        return redirect('preferences')

    return render(request, 'core/preferences.html', {'user': active_user, 'pref': pref})
# for testing
def StartQueue(request):
    # testing a crontab
    # youtoken = UserYoutubeToken.objects.get(user=request.user)
    creds = youtube_request( request.user.id )
    
    
    return redirect("/") 
#===================================================
def fbCronJobStart(request):
    pageid = json.loads(request.body.decode('utf-8')).get('pageid')
    print(pageid)
    if pageid:
        try:
            facebook_Pagetoken.objects.get(id=pageid)
        except facebook_Pagetoken.DoesNotExist:
            return JsonResponse({"Response" : "Page Does Not Exists"})
        # create or get a IntervalSchedule
        schedule, created = IntervalSchedule.objects.get_or_create(every=1, period=IntervalSchedule.MINUTES,)
        print(schedule , created)
        #create or get 
        task , created = PeriodicTask.objects.get_or_create(name='pageid_fb'+str(pageid),interval=schedule, task='dashboard.facebook_task.facebook_Custom_PageData', args=json.dumps([pageid]))
        # Sample For Example
        # PeriodicTask.objects.(interval=schedule,  task='proj.tasks.import_contacts',args=json.dumps(['arg1', 'arg2']),kwargs=json.dumps({'be_careful': True,}),expires=datetime.utcnow() + timedelta(seconds=30))
        if created:
            task.expires = datetime.utcnow() + timedelta(minutes = 10)
            task.start_time = datetime.utcnow()
            task.save()
            return JsonResponse({"Response" : "Done"})
        else:
            return JsonResponse({"Response" : "Already Running<br>Expires at "+str(task.expires)})
    return JsonResponse({"Response" : "No Page Selected"})

