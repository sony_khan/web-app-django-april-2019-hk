from dashboard.models import UserYoutubeToken, YoutubeChannels, YoutubeVideos,YoutubeTopLevelComments,YoutubeSubComments
from celery import shared_task
import requests , json ,ast
from django.conf import settings
import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery
import  dashboard.utils as api

# Get Youtube Authrized api object from credentials 
def getAutharizedBuilder(creds):
	creds = ast.literal_eval(creds)
	credentials = google.oauth2.credentials.Credentials(**creds)
	return googleapiclient.discovery.build(settings.API_SERVICE_NAME, settings.API_VERSION, credentials=credentials,cache_discovery=False)
# Call after saving credentials of user to save a channel and related data
def youtube_request(user_id):
	# Save all channels in Database
	get_youtube_channel(user_id)
	channels = YoutubeChannels.objects.filter(user_id = user_id)
	print("Channels fetched")

	for channel in channels:
		# For Each channel Save all videos in Database
		get_youtube_videos(channel.user_id,channel.youtube_id,maxResults=10)
		allvideos = YoutubeVideos.objects.filter(channel = channel)
		# For each video save all comment threads(top level and replies (if any)) in Database
		for video in allvideos:
			get_youtube_commentThread(user_id,video.id)
		print("Comments fetched")

# Youtube Api Request for channels
def get_youtube_channel(userid , mine =True ,  maxResults=50):
	try:
		token = UserYoutubeToken.objects.get(user_id = userid)
		youtube = getAutharizedBuilder(token.creds)
		result = youtube.channels().list(part="id,snippet,status,contentDetails",
			maxResults=maxResults,
			mine=mine,
			fields="pageInfo,items(id,contentDetails/relatedPlaylists/uploads,snippet(title,description,publishedAt,thumbnails/default/url),status/privacyStatus)").execute()
		for item in result["items"]:
			saveAChannel(item, userid)
	except Exception as e:
		print("Error")
		print(e)
# Save Youtube Api Response of channels list
def saveAChannel(item,userid):
	try:
		channel, created = YoutubeChannels.objects.get_or_create(user_id = userid , youtube_id=item.get("id",""))
		channel.title = item.get("snippet",{}).get("title","")
		channel.description = item.get("snippet",{}).get("description", "")
		channel.imurlage = item.get("snippet",{}).get("thumbnails",{}).get("default",{}).get("url",'')
		channel.status = item.get("status",{}).get("privacyStatus","")
		channel.uploads_id =  item.get("contentDetails",{}).get("relatedPlaylists",{}).get("uploads",'')
		channel.save()
	except Exception as e:
		print("Error Saving a channel")
		print(e)

# Youtube Api Request for videos
def get_youtube_videos(userid, channelid ,  maxResults=50):
	try:
		token = UserYoutubeToken.objects.get(user_id = userid)
		youtube = getAutharizedBuilder(token.creds)
		channel = YoutubeChannels.objects.get(user_id = userid, youtube_id = channelid)
		# result = youtube.channels().list(part="contentDetails",id=channelid,maxResults=maxResults).execute()
		result = youtube.playlistItems().list(
			part="id,snippet,status",
			playlistId=channel.uploads_id,
			maxResults=maxResults,
			fields="pageInfo,items(id,snippet(publishedAt,title,description,thumbnails/default/url,resourceId/videoId))").execute()
		# print(result)
		for item in result["items"]:
			saveAVideo(item, channel.id)
	except Exception as e:
		print("Error")
		print(e)

# Save Youtube Api Response of Video list
def saveAVideo(item, channelid):
	try:
		print(item)
		video , created = YoutubeVideos.objects.get_or_create(channel_id = channelid, video_id = item.get("snippet",{}).get("resourceId",{}).get("videoId",""))
		video.playlistid = item.get("id","")
		video.title = item.get("snippet",{}).get("title","")
		video.description = item.get("snippet",{}).get("description","")
		video.publishedAt = item.get("snippet",{}).get("publishedAt","")
		video.image = item.get("snippet",{}).get("thumbnails",{}).get("default",{}).get("url","")
		video.save()
	except Exception as e:
		print("Error while saving video" )
		print(e)
# Youtube Api Request for comment threads (toplevel comment  and replies)
def get_youtube_commentThread(userid, vidoid ,  maxResults=100, moderationStatus="published"):
	try:
		token = UserYoutubeToken.objects.get(user_id = userid)
		video = YoutubeVideos.objects.get(id = vidoid)
		youtube = getAutharizedBuilder(token.creds)
		result = youtube.commentThreads().list(
			part="id,snippet,replies",
			textFormat="plainText",
			order = 'time',
			videoId=video.video_id,
			maxResults=maxResults,
			fields= "nextPageToken,pageInfo,items(snippet(topLevelComment(id,snippet(authorDisplayName,authorProfileImageUrl,textDisplay,likeCount,updatedAt))),replies/comments(id,snippet(authorDisplayName,authorProfileImageUrl,textDisplay,likeCount,updatedAt)))",
			moderationStatus=moderationStatus).execute()
		# print(result)
		for commentThread in result["items"]:
			saveAcommentThread(commentThread, vidoid)

	except Exception as e:
		print("Error")
		print(e)

# Save Youtube Api Response of comment thread list (all top level comments)
def saveAcommentThread(commentThread , videoId):
	try:
		comment, created = YoutubeTopLevelComments.objects.get_or_create(video_id = videoId , commentid = commentThread.get("snippet",{}).get("topLevelComment",{}).get("id",""))
		comment.name = commentThread.get("snippet",{}).get("topLevelComment",{}).get("snippet",{}).get("authorDisplayName","")
		comment.image =commentThread.get("snippet",{}).get("topLevelComment",{}).get("snippet",{}).get("authorProfileImageUrl","")
		comment.message = commentThread.get("snippet",{}).get("topLevelComment",{}).get("snippet",{}).get("textDisplay","")
		comment.likes =commentThread.get("snippet",{}).get("topLevelComment",{}).get("snippet",{}).get("likeCount","")
		comment.time = commentThread.get("snippet",{}).get("topLevelComment",{}).get("snippet",{}).get("updatedAt","")
		comment.save()
		if(commentThread.get("replies",False)):
			for subcomment in commentThread.get("replies",{}).get("comments",[]):
				saveAsubComment(comment.id , subcomment)

	except Exception as e:
		print("Error in saveing Comment Thread") 
		print(e)
# Save Youtube Api Response of comment replies list 
def saveAsubComment(topLevelCommentID , subcomment):
	try:
		comment , created = YoutubeSubComments.objects.get_or_create(top_comment_id = topLevelCommentID, commentid = subcomment.get("id",""))
		comment.name = subcomment.get("snippet",{}).get("authorDisplayName","")
		comment.image =subcomment.get("snippet",{}).get("authorProfileImageUrl","")
		comment.message = subcomment.get("snippet",{}).get("textDisplay","")
		comment.likes =subcomment.get("snippet",{}).get("likeCount","")
		comment.time = subcomment.get("snippet",{}).get("updatedAt","")
		comment.save()
	except Exception as e:
		print("Error in saveing SubComment Thread")
		print(e)