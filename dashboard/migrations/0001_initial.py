# Generated by Django 2.1.1 on 2019-07-23 19:41

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='facebook_Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment_id', models.CharField(max_length=255)),
                ('created_at', models.CharField(max_length=255)),
                ('message', models.TextField()),
                ('comment_user_name', models.CharField(max_length=255)),
                ('comment_user_image', models.TextField(max_length=255)),
                ('comment_user_id', models.CharField(max_length=255)),
                ('comment_likes', models.CharField(max_length=255)),
                ('language', models.CharField(blank=True, max_length=255)),
                ('crisis', models.CharField(blank=True, max_length=255)),
                ('intent', models.CharField(blank=True, max_length=255)),
                ('toxic', models.CharField(blank=True, max_length=255)),
                ('gender', models.CharField(blank=True, max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='facebook_Pagetoken',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('page_access_token', models.CharField(max_length=255)),
                ('page_name', models.CharField(max_length=255)),
                ('page_id', models.CharField(max_length=255)),
                ('page_category', models.CharField(max_length=255)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='facebook_Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('post_user_id', models.CharField(max_length=255)),
                ('message', models.TextField()),
                ('post_id', models.CharField(max_length=255)),
                ('created_at', models.CharField(max_length=255)),
                ('post_user_name', models.CharField(max_length=512)),
                ('media_urls', models.TextField()),
                ('likes', models.CharField(max_length=255)),
                ('shares', models.CharField(max_length=255)),
                ('commentcount', models.CharField(max_length=255)),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashboard.facebook_Pagetoken')),
            ],
        ),
        migrations.CreateModel(
            name='Preferences',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timezone', models.CharField(db_column='timezone', default='GMT +05 (ISL)', max_length=255)),
                ('theme', models.CharField(db_column='theme', default='dark', max_length=255)),
                ('lang', models.CharField(db_column='lang', default='en', max_length=255)),
                ('limit_range', models.CharField(db_column='limit_range', default='20', max_length=255)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_column='title', max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('post_ids', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashboard.Project')),
                ('user', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='twitter_replies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
                ('reply_id', models.CharField(max_length=255)),
                ('created_at', models.CharField(max_length=255)),
                ('reply_user_name', models.CharField(max_length=255)),
                ('reply_user_ScreenName', models.CharField(max_length=512)),
                ('reply_user_image', models.CharField(max_length=255)),
                ('reply_user_id', models.CharField(max_length=255)),
                ('likes', models.CharField(max_length=255)),
                ('language', models.CharField(blank=True, max_length=255)),
                ('crisis', models.CharField(blank=True, max_length=255)),
                ('intent', models.CharField(blank=True, max_length=255)),
                ('toxic', models.CharField(blank=True, max_length=255)),
                ('gender', models.CharField(blank=True, max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='twitter_tweet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tweet_id', models.CharField(max_length=255)),
                ('text', models.TextField()),
                ('created_at', models.CharField(max_length=255)),
                ('tweet_user_id', models.CharField(max_length=255)),
                ('tweet_user_name', models.CharField(max_length=512)),
                ('tweet_user_ScreenName', models.CharField(max_length=512)),
                ('tweet_user_picture', models.CharField(max_length=255)),
                ('likes', models.CharField(blank=True, max_length=255)),
                ('language', models.CharField(blank=True, max_length=255)),
                ('crisis', models.CharField(blank=True, max_length=255)),
                ('intent', models.CharField(blank=True, max_length=255)),
                ('toxic', models.CharField(blank=True, max_length=255)),
                ('gender', models.CharField(blank=True, max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='User_fb_token',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('access_token', models.CharField(max_length=255)),
                ('fb_user_name', models.CharField(max_length=255)),
                ('fb_user_email', models.CharField(max_length=255)),
                ('fb_user_picture', models.CharField(max_length=255)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='User_twitter_token',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('access_key', models.CharField(max_length=255)),
                ('access_secret', models.CharField(max_length=255)),
                ('twitter_user_id', models.CharField(max_length=255)),
                ('twitter_user_name', models.CharField(max_length=255)),
                ('twitter_user_ScreenName', models.CharField(max_length=255)),
                ('twitter_user_picture', models.CharField(max_length=255)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserYoutubeToken',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creds', models.CharField(blank=True, max_length=5000)),
                ('token', models.CharField(blank=True, max_length=5000)),
                ('name', models.CharField(blank=True, max_length=255)),
                ('image', models.CharField(blank=True, max_length=255)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='YoutubeChannels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('youtube_id', models.CharField(blank=True, max_length=255)),
                ('title', models.CharField(blank=True, max_length=255)),
                ('description', models.CharField(blank=True, max_length=255)),
                ('imurlage', models.CharField(blank=True, max_length=255)),
                ('status', models.CharField(blank=True, max_length=255)),
                ('uploads_id', models.CharField(blank=True, max_length=255)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='YoutubeSubComments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('commentid', models.CharField(blank=True, max_length=255)),
                ('name', models.CharField(blank=True, max_length=255)),
                ('image', models.CharField(blank=True, max_length=255)),
                ('message', models.CharField(blank=True, max_length=5000)),
                ('likes', models.CharField(blank=True, max_length=255)),
                ('time', models.CharField(blank=True, max_length=255)),
                ('language', models.CharField(blank=True, max_length=255)),
                ('crisis', models.CharField(blank=True, max_length=255)),
                ('intent', models.CharField(blank=True, max_length=255)),
                ('toxic', models.CharField(blank=True, max_length=255)),
                ('gender', models.CharField(blank=True, max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='YoutubeTopLevelComments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('commentid', models.CharField(blank=True, max_length=255)),
                ('name', models.CharField(blank=True, max_length=255)),
                ('image', models.CharField(blank=True, max_length=255)),
                ('message', models.CharField(blank=True, max_length=5000)),
                ('likes', models.CharField(blank=True, max_length=255)),
                ('time', models.CharField(blank=True, max_length=255)),
                ('language', models.CharField(blank=True, max_length=255)),
                ('crisis', models.CharField(blank=True, max_length=255)),
                ('intent', models.CharField(blank=True, max_length=255)),
                ('toxic', models.CharField(blank=True, max_length=255)),
                ('gender', models.CharField(blank=True, max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='YoutubeVideos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('video_id', models.CharField(blank=True, max_length=255)),
                ('playlistid', models.CharField(blank=True, max_length=255)),
                ('title', models.CharField(blank=True, max_length=255)),
                ('description', models.CharField(blank=True, max_length=5000)),
                ('publishedAt', models.CharField(blank=True, max_length=255)),
                ('image', models.CharField(blank=True, max_length=255)),
                ('channel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashboard.YoutubeChannels')),
            ],
        ),
        migrations.AddField(
            model_name='youtubetoplevelcomments',
            name='video',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashboard.YoutubeVideos'),
        ),
        migrations.AddField(
            model_name='youtubesubcomments',
            name='top_comment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashboard.YoutubeTopLevelComments'),
        ),
        migrations.AddField(
            model_name='twitter_tweet',
            name='twitter_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashboard.User_twitter_token'),
        ),
        migrations.AddField(
            model_name='twitter_replies',
            name='tweet',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashboard.twitter_tweet'),
        ),
        migrations.AddField(
            model_name='facebook_comment',
            name='post',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashboard.facebook_Post'),
        ),
    ]
