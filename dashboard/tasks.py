import string
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from dashboard.models import Project,UserYoutubeToken
from celery import shared_task
import requests
from aylienapiclient import textapi
import json
import datetime
from dashboard.utils import get_api, getLanguage, getSentiment, getUserGender, getIntent, getToxic, getCrisis
import sys
from email.utils import parsedate_tz
from dashboard import views
from accounts.models import User




@shared_task
def fetchYouData(userid):
    print("Here : before fetch")
    print(userid)
    youauth = UserYoutubeToken.objects.get(user_id=userid)
    print("Here : in fetch")
    user = User.objects.get(id=userid)
    results = views.youtube_request(youauth.creds)
    print (results)
    for channel in results['channels']:
        for playlist in channel['playLists']:
            for video in playlist['videos']:
                for com in video['comments']:
                    if not Comment.objects.filter(comment_id=com['id']).exists():
                        newcom = Comment()
                        newcom.message = com['textDisplay']
                        newcom.source = 'youtube'
                        newcom.comment_id = com['id']
                        # ts = int(com['created_time'])
                        # newcom.created_at = datetime.utcfromtimestamp(
                        #     ts).strftime('%Y-%m-%d %H:%M:%S')
                        newcom.created_at = com['publishedAt']
                        # newcom.language = getLanguage(com['textDisplay'])
                        newcom.language = 'en'
                        newcom.sentiment = 'Positive'
                        # newcom.sentiment = getSentiment(
                        #                     com['textDisplay'], newcom.language)
                        newcom.user_name = com['authorDisplayName']
                        newcom.com_user_id = "123"
                        # newcom.user_name = 'something'
                        newcom.user = user
                        newcom.likes = '111'
                        # newcom.user_image = 'new'
                        newcom.user_image = com['authorProfileImageUrl']
                        newcom.user_followers = '123'
                        newcom.gender = 'male'
                        # newcom.gender = getUserGender(com['authorDisplayName'])
                        # newcom.gender = getUserGender('hameed')
                        newcom.is_toxic = 'Toxic'
                        newcom.is_intent = 'True'
                        newcom.is_crisis = 'Problematic'
                        # newcom.is_toxic = getToxic(com['textDisplay'])
                        # newcom.is_intent = getIntent(com['textDisplay'])
                        # newcom.is_crisis = getCrisis(com['textDisplay'], newcom.language)
                        newcom.save()

@shared_task
def create_random_user_accounts(total):
    for i in range(total):
        username = 'user_{}'.format(
            get_random_string(10, string.ascii_letters))
        email = '{}@example.com'.format(username)
        password = get_random_string(50)
        User.objects.create_user(
            username=username, email=email, password=password)
    return '{} random users created with success!'.format(total)