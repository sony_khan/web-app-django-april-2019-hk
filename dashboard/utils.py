import requests
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six
from aylienapiclient import textapi
import json
import tweepy
from django.db.models import Count
from email.utils import parsedate_tz
from datetime import datetime, timedelta
from django.conf import settings
import ast
import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery

def get_api(access_token, access_secret):
    # set up and return a twitter api object
#    oauth = tweepy.OAuthHandler('mqjmf3Tp4D8NGNDd5AR9dHKrT', 'd3uPKttcEBYLPeyyrLIFRi45KzPCKcgeEMYs8kAo00gFk5egDD')
    oauth = tweepy.OAuthHandler(settings.CONSUMER_KEY, settings.CONSUMER_SECRET)
    access_key = access_token
    access_secret = access_secret
    oauth.set_access_token(access_key, access_secret)
    api = tweepy.API(oauth)
    return api


def getLanguage(text):
    try:
        requrl = "http://95.216.2.224:5033/detect-language?text={}".format(text)
        resp = requests.get(requrl).json()
        if(resp['status'] != False):
            return resp['output']
        else:
            return 'en'
    except Exception as e:
        print(e)
        return ''


def getToxic(text):
    try:
        requrl = "http://95.216.2.224:5033/toxic?comment={}".format(text)
        resp = requests.get(requrl).json()
        return resp['is_toxic']
    except Exception as e:
        print(e)
        return ''


def getIntent(text):
    try:
        requrl = "http://95.216.2.224:5033/intent?comment={}".format(text)
        resp = requests.get(requrl).json()
        return resp['is_intent']
    except Exception as e:
        print(e)
        return ''

# TODO test verification
def getCrisis(text, lang):
    try:
        requrl = "http://95.216.2.224:9002/crisis?comment={}&lang={}".format(text, lang)            
        resp = requests.get(requrl).json()
        return resp['output']
    except Exception as e:
        print(e)
        return ''
    

def getSentiment(text, lang):
    requrl = "http://95.216.2.224:9000/sentiment/?comment={}&format=json&lan={}".format(
        text, lang)
    resp = requests.get(requrl).json()
    if lang == 'fr' or lang == 'arz' or lang == 'ar':
        return resp['Comment:Class']
    else:
        sent = float(resp['Comment:Class'])
        if sent < 0:
            return 'Negative'
        elif sent > 0:
            return 'Positive'
        else:
            return 'Nuetral'


def getUserPages(accessToken):
    pages = requests.get(
        'https://graph.facebook.com/me/accounts?access_token='+accessToken).json()
    if pages.get('error', False):
        raise Exception('facebook')
    return pages.get('data')


def getUserGender(user_name):
    appurl = 'http://95.216.2.224:5033/gender?name={}'.format(user_name)
    pages = requests.get(appurl).json()
    if pages.get('error', False):
        raise Exception('facebook')
    return pages['gender']


def getPageDetail(id, token):
    page = requests.get(
        'https://graph.facebook.com/'+id+'?access_token=' + token+'&fields=name,access_token,category,about,bio,contact_address,description,location,website,single_line_address').json()
    if page.get('error', False):
        raise Exception('facebook')
    return page


def getPagePosts(id, token):
    page = requests.get(
        'https://graph.facebook.com/'+id+'/posts?access_token=' + token+'&fields=name,description,picture,caption').json()
    if page.get('error', False):
        raise Exception('facebook')
    return page.get('data')


def getPostComments(id, token):
    page = requests.get(
        'https://graph.facebook.com/'+id+'/comments?access_token=' + token+'&fields=from,message,attachment,created_time').json()
    if page.get('error', False):
        raise Exception('facebook')
    return page.get('data')


def genSentChart(comments):
    allcoms = comments.extra(select={'t': 'date( created_at )'}).values('t') \
        .annotate(y=Count('created_at'))
    alldates = list()
    for com in allcoms:
        alldates.append(com['t'].strftime('%Y-%m-%d'))
    poscoms = comments.filter(sentiment='Positive')
    negcoms = comments.filter(sentiment='Negative')

    poscoms = poscoms.extra(select={'t': 'date( created_at )'}).values('t') \
        .annotate(y=Count('created_at'))

    negcoms = negcoms.extra(select={'t': 'date( created_at )'}).values('t') \
        .annotate(y=Count('created_at'))

    poscomlist = list()
    for com in poscoms:
        poscomlist.append({'t': com['t'].strftime('%Y-%m-%d'), 'y': com['y']})

    negcomlist = list()
    for com in negcoms:
        negcomlist.append({'t': com['t'].strftime('%Y-%m-%d'), 'y': com['y']})

    alldata = dict()
    alldata['dates'] = alldates
    alldata['positivecoms'] = poscomlist
    alldata['negativecoms'] = negcomlist

    return alldata

def genIntentChart(comments):
    allcoms = comments.extra(select={'t': 'date( created_at )'}).values('t') \
        .annotate(y=Count('created_at'))
    alldates = list()
    for com in allcoms:
        alldates.append(com['t'].strftime('%Y-%m-%d'))

    intcomslist = list()
    for com in allcoms:
        intcomslist.append({'t': com['t'].strftime('%Y-%m-%d'), 'y': com['y']})

    alldata = dict()
    alldata['dates'] = alldates
    alldata['intcoms'] = intcomslist

    return alldata

def genCrisisChart(comments):
    allcoms = comments.extra(select={'t': 'date( created_at )'}).values('t') \
        .annotate(y=Count('created_at'))
    alldates = list()
    for com in allcoms:
        alldates.append(com['t'].strftime('%Y-%m-%d'))

    cricomslist = list()
    for com in allcoms:
        cricomslist.append({'t': com['t'].strftime('%Y-%m-%d'), 'y': com['y']})

    alldata = dict()
    alldata['dates'] = alldates
    alldata['cricoms'] = cricomslist

    return alldata

def genToxicChart(comments):
    allcoms = comments.extra(select={'t': 'date( created_at )'}).values('t') \
        .annotate(y=Count('created_at'))
    alldates = list()
    for com in allcoms:
        alldates.append(com['t'].strftime('%Y-%m-%d'))

    toxcomslist = list()
    for com in allcoms:
        toxcomslist.append({'t': com['t'].strftime('%Y-%m-%d'), 'y': com['y']})

    alldata = dict()
    alldata['dates'] = alldates
    alldata['toxcoms'] = toxcomslist

    return alldata

def to_datetime(datestring):
    time_tuple = parsedate_tz(datestring.strip())
    dt = datetime(*time_tuple[:6])
    return dt - timedelta(seconds=time_tuple[-1])

def credentials_to_dict(credentials):
    return {'token': credentials.token,
            'refresh_token': credentials.refresh_token,
            'token_uri': credentials.token_uri,
            'client_id': credentials.client_id,
            'client_secret': credentials.client_secret,
            'scopes': credentials.scopes}
class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.is_active)
        )


account_activation_token = TokenGenerator()
