from django import template

register = template.Library()

@register.simple_tag
def getcompercent(total, cris):
    return round((cris/total)*100,2)

@register.simple_tag
def filtercomment(coms,postid):
    return coms.filter(post_id=postid)