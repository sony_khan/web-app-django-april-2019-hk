from dashboard.models import facebook_Pagetoken, facebook_Comment, facebook_Post
from celery import shared_task
import requests
from datetime import datetime, timedelta
import  dashboard.utils as api

# task run to get pages data
@shared_task
# api get data we needed from fb pages and save into database.
#  limit=2, since=(datetime.now() - timedelta(2)).strftime('%Y-%m-%d'), until=(datetime.now()).strftime('%Y-%m-%d')
def facebook_Custom_PageData(page_id):

    # getting all user pages info data from facebook_Pagetoken table
    pages = facebook_Pagetoken.objects.filter(id=page_id)
    
    for page in pages:
        # we are passing page id and access token and accessing page data
        tokenurl = "https://graph.facebook.com/{0}/feed?date_format=U&access_token={1}&fields=from,message,created_time,picture".format(page.page_id,  page.page_access_token)
        # below two code lines will be removed if recursive logic needs to activate
        # gettin data in json formatt
        r = requests.get(tokenurl).json()
        # function will save data in database and also check for duplication
        Save_Page_post_data(r, page)
        # recursive call strategy will be done here to get data till end
        # get_posts(tokenurl,page)
        # if more posts will be availabe if condition becomes true
    return 'fetched all data for facebook user'

# task run to get pages data
@shared_task
# api get data we needed from fb pages and save into database.
def facebook_PageData(userid):
    # getting all user pages info data from facebook_Pagetoken table
    pages = facebook_Pagetoken.objects.filter(user_id=userid)
    for page in pages:
        # we are passing page id and access token and accessing page data
        tokenurl = "https://graph.facebook.com/{0}/feed?date_format=U&access_token={1}&fields=from,message,created_time,picture".format(page.page_id, page.page_access_token)
        # below two code lines will be removed if recursive logic needs to activate
        # gettin data in json formatt
        r = requests.get(tokenurl).json()
        # function will save data in database and also check for duplication
        Save_Page_post_data(r, page)
        # recursive call strategy will be done here to get data till end
        # get_posts(tokenurl,page)
        # if more posts will be availabe if condition becomes true
    return 'fetched all data for facebook user'


# save post data in db and start comment access token
def Save_Page_post_data(postData,page):
    # looping through feed data
    for post in postData['data']:
        # if a post already exist in database, we'll just save it's instance in variable
        if facebook_Post.objects.filter(post_id=post['id']).exists():
            compost = facebook_Post.objects.get(post_id=post['id'])
        else:
            # a request will go for each post id to get actions data...
            likeshareurl = "https://graph.facebook.com/v3.3/{0}?fields=shares,likes.summary(true),comments.summary(true)&access_token={1}".format(post['id'],page.page_access_token)
            postlikeshare = requests.get(likeshareurl).json()
            # a new post instance created and save into db
            newpost = facebook_Post()
            # Assign in json response to model
            newpost.page = page
            ts = int(post.get('created_time',None))
            newpost.created_at = datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
            newpost.post_user_name =  post.get('from',{}).get('name',' ')
            newpost.post_user_id =  post.get('from',{}).get('id',' ')
            newpost.message = post.get('message',' ')
            newpost.media_urls = post.get('picture', ' ')
            newpost.post_id = post.get('id',' ')
            newpost.likes = postlikeshare.get('likes',{}).get('summary',{}).get('total_count',0)
            newpost.shares = postlikeshare.get('shares',{}).get('count',0)
            newpost.commentcount = postlikeshare.get('comments',{}).get('summary',{}).get('total_count',0)
            newpost.save()
            compost = newpost
            # here we pass post id to url to access all comments data...
        tokenurl = "https://graph.facebook.com/" + compost.post_id +"/comments?date_format=U&limit=30&access_token="+page.page_access_token +"&fields=from{name,picture},message,created_time,like_count"
        # below code till 66 lines will be removed if recursive logic needs to activate
        # getting comments data
        # comments = requests.get(tokenurl).json()
        # this function save data into database table dashboard_comment
        # Save_Post_comment(comments,compost)

        get_comments(tokenurl,page,compost)
    return 'data save successfully'

# get all availabe comments on each post
def get_comments(tokenurl,page,compost):
    # getting comments data
    comments = requests.get(tokenurl).json()
    # this function save data into database table dashboard_comment
    Save_Post_comment(comments,compost)
    # if next page link will be able , recusion happen here
    if(comments.get('paging', {}).get('next', False)!= False):
        nexturl = comments['paging'].get('next')
        get_comments(nexturl, page, compost)
    return 'comments data fetch'

# save fb comments data into db
def Save_Post_comment(comments, compost):
    # looping through comments data
    for com in comments['data']:
        # if a comment have no text message  just ignore it
        if not com['message']:
            pass
        else:
            # if comment not available in database,we make new comment instance and save commnet in
            # dashboard_comment table
            if not facebook_Comment.objects.filter(comment_id=com['id']).exists():
                newcom = facebook_Comment()
                newcom.post = compost
                newcom.comment_user_name = com.get('from',{}).get('name','')
                newcom.comment_user_id = com.get('from',{}).get('id','')
                newcom.comment_user_image = com.get('from',{}).get('picture',{}).get('data',{}).get('url', '')
                ts = int(com.get('created_time', None))
                newcom.created_at = datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                newcom.message = com.get('message',' ')
                newcom.comment_id = com.get('id', '')
                newcom.comment_likes = com.get('like_count', 0)
                newcom.save()
                if(newcom.comment_id):
                    analysis.delay(newcom.comment_id)
    return 'data saved successfully'


    # get all posts of fb page
def get_posts(tokenurl, page):
    # gettin data in json formatt
    r = requests.get(tokenurl).json()
    # function will save data in database and also check for duplication
    Save_Page_post_data(r, page)
    # if next page link will be able , recusion happen here
    if(r.get('paging', {}).get('next', False)!= False):
        nexturl = r['paging'].get('next')
        get_posts(nexturl, page)
    return "all data fetched"


@shared_task
#comment id is fb comment id
def analysis(commentID):
    if commentID:
        print("intent analysis started")
        comment = facebook_Comment.objects.filter(comment_id=commentID)[0]
        print(comment)
        comment.language = api.getLanguage(comment.message)
        comment.crisis = api.getCrisis(comment.message , comment.language)
        comment.intent = api.getIntent(comment.message)
        comment.gender = api.getUserGender(comment.comment_user_name)
        comment.toxic = api.getToxic(comment.message)

        comment.save()


        # comment.update(gender = gender,toxic = toxic,intent = intent,crisis = crisis,language = language)
        # print(language,crisis, intent, gender, toxic)
        print("intent analysis Complete for id " + commentID)
    else:
        print("No Comment ID")