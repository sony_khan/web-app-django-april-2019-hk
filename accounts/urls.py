from django.urls import path, re_path
from accounts import views as account_views
from django.conf.urls import url

urlpatterns = [
    # login url
    path('login/', account_views.ManualLoginView.as_view(), name='login'),
    # signup url
    path('signup/', account_views.SignUpView.as_view(), name='signup'),
    # path to request reset email
    path('reset/', account_views.ResetView.as_view(), name='reset'),
    # password reset email verification
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        account_views.reset_confirm, name='password_reset_confirm'),
    # path to change password form
    url(r'^change/password/$', account_views.ChangePasswordView.as_view(), name='change-password'),

    re_path('activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        account_views.activate, name='activate'),
    # logout url
    path('logout/', account_views.Logout, name='logout'),
    #account activation url
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        account_views.activate, name='activate'),
]
